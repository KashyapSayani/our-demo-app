package com.example.myapplication;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class DisplayActivity extends AppCompatActivity {

    ListView lvUsers;
    Spinner spUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

//    LinearLayout llMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_activity);
        initViewReference();
        bindViewValues();
    }

    void bindViewValues() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
//        for (int i = 0; i < userList.size(); i++) {
//            View view = LayoutInflater.from(SecondActivity.this).inflate(R.layout.view_row_user_list, null);
//
//            TextView tvName = view.findViewById(R.id.tvLstName);
//            TextView tvEmail = view.findViewById(R.id.tvLstEmail);
//            TextView tvGender = view.findViewById(R.id.tvLstGender);
//
//            tvName.setText(userList.get(i).get(Const.FIRST_NAME) + " " + userList.get(i).get(Const.LAST_NAME));
//            tvGender.setText(userList.get(i).get(Const.GENDER).toString());
//            tvEmail.setText(String.valueOf(userList.get(i).get(Const.EMAIL_ADDRESS)));
//            llMain.addView(view);
//        }

        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
        /*spUsers.setAdapter(userListAdapter);*/
/*
        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Toast.makeText(DisplayActivity.this, userList.get(spUsers.getSelectedItemPosition()).get(Const.NAME).toString(), Toast.LENGTH_SHORT).show();
            }
        });*/

        /*spUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(DisplayActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

    }

    void initViewReference() {
//        llMain = findViewById(R.id.lvActUserList);

        /*spUsers = findViewById(R.id.spActUsers);*/
        lvUsers = findViewById(R.id.lvActUserList);
        getSupportActionBar().setTitle(R.string.lbl_display_activity);
    }
}